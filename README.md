# README #

This is the instruction for this repository.

### What is this repository for? ###

This repository is for the online store developed for an equipment selling organisation, it is written in JavaScript and node.js
Version 1.0
The full tutorials will be available on (https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Kindly initiate the server from visual studio codes, open a terminal and type "node index.js" in the terminal.
To configure, please open http://localhost:8080 from your browser
The dependencies include express, express-validator, body-parser and ejs.
This application is built with noSQL
Tests are ongoing
Deployment is pending

### Contribution guidelines ###

The test cases have been completely developed
Code review is done weekly

### Who do I talk to? ###

Please contact bunmioluwa@gmail.com for more details about the repository or any further clarifications.
